﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public Transform player;
    private Vector3 _cameraOffSet;
    [Range(0.01f, 1.0f)]
    public float SmoothFactor = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        //aixo mira la distancia de la camera amb el player i ho guarda 
        _cameraOffSet = transform.position - player.position;

    }

    // Update is called once per frame
    void Update()
    {
        //aixode aqui guarda la distancia on sha mogut el personatje
        Vector3 newPos =new Vector3( player.transform.position.x + _cameraOffSet.x, 0, -4);
        //aixo mou la camera a la nova posicio, el smoothfactor es lavelocitat en la que es mou la camera en direccio
        transform.position = Vector3.Slerp(transform.position, newPos, SmoothFactor);
    }
}
