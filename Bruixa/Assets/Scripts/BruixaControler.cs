﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BruixaControler : MonoBehaviour
{
    

    private Rigidbody2D rb2d; //aixo es per les fisiques de la bruixa
    public Animator anim;      //aixo es per les animacions 
    [Header("Variables")]
    public float speed;        //Aixo Controla la velocitat de la bruixa
    public float jump;          //amb aixo controles la força amb la que salta
    public bool facingRight;  // aixo retorna true o false, i es per saber si estem mirant a la dreta o no
    private bool attack;    //per saber si estem atacant en aquell instant
    public int currentCoins; //el numero de monedes que te
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();//es guarda el objecte rigidbody (fisiques del player)
        anim = GetComponent<Animator>();//guarda les animacions del player
        facingRight = true;
    }

    private void Update()
    {
        Inputs();
    }
    private void FixedUpdate()
    {
        //Aixo es per guardar el moviment horitzontal de la bruixa
        //si cliquem "A" o "D" o les "<-" o "->"
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        //movement crida el void de més abaix de Movement
        //flip crida void Flip...
        Movement(moveHorizontal, moveVertical);
        Flip(moveHorizontal);
        Attack();
        ResetValues();
   
    }

    private void Movement(float moveHorizontal, float moveVertical)
    {
        //la condicio if ens mira si sesta executant "Attack", si no ho esta entra dintre l'if
        //i ens mou el personatje amb una "speed" (velocitat) que li direm nosaltres
        if (!this.anim.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))
        {
            //es un vector que te una "X" i  una "Y" i multiplica per speed per moures més rapid o méslent
            rb2d.velocity = new Vector2(moveHorizontal * speed, moveVertical * speed);
        }
        
        //activa la animació de correr
        anim.SetFloat("speed",Mathf.Abs( moveHorizontal));//si la velocitat es mes gran que 0 activara animacio de correr
        //en cas que moveHoritzontal sigui 0, vol dir que no es mou, activara animació de idel, esta quiet
    }
    private void Attack()
    {
        //aixo mira si estem atacan, si estem atacant, no passa res
        //si no estem atacant, atacara
        if (attack && !this.anim.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))
        {
            anim.SetTrigger("attack");//animacio de atacar
            rb2d.velocity = Vector2.zero;//si estem atacant no ens podrem moure
        }
        
    }
    private void Inputs()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            attack = true;//quan cliquem el boto esquerra del ratoli atacara
        }
    }
    private void Flip(float moveHorizontal)
    {
        //aixo ens mira si estem mirant a la dreta o esquerra. esquerra 1 dreta -1
        //si estem mirant a la esquerra i estem mirant a la dreta, el personatje girara 
        //i mirara a la esquerra. i a la inversa igual
        if(moveHorizontal > 0 && !facingRight || moveHorizontal < 0 && facingRight)
        {
            facingRight = !facingRight;

            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            theScale.z = 1;
            transform.localScale = theScale;
        }
    }
    private void ResetValues()
    {
        attack = false;
    }
   public int CurrentCoins()
    {
        return currentCoins;
    }
    public void UpdateCoins(int coinn)
    {
        currentCoins = currentCoins + coinn;
    }
}
