﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coins : MonoBehaviour
{
    BruixaControler player;
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<BruixaControler>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.CompareTag("player"))//si colisiona amb el player...
        {
            player.UpdateCoins(1);//aixo envia uun 1 i es el que suma de punts
            Destroy(gameObject);//destrueix el objecte
        }
    }
}
