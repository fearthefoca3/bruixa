﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDcontroller : MonoBehaviour
{
    public int coins;
    public Text textcoins;
    BruixaControler player;
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<BruixaControler>();
    }

    // Update is called once per frame
    void Update()
    {
        //aixo actualitza les monedes de adalt de tot de la pantlla
        // agafa les monedes i les actualitza
        textcoins.text = string.Format(""+player.CurrentCoins());
    }
}
